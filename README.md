# Challenge: Progress Stepper #

Showing progress visually is very handy in UI design, but it’s even more important to make sure that it’s implemented semantically too. In this challenge, that’s the goal!

### What you’re building ###

![Scheme](https://piccalilli.imgix.net/images/blog/fecc/challenge-008.jpg)


### The Brief ###

Take the design assets and build a progress stepper component, using whatever technology you like.

You should aim for the following, at a minimum:

* Your solution uses semantic HTML
* Your solution could work with a handful of items or a lot of items
* Long items and short items should work harmoniously
* Responsive and works in various browsers
* Make it accessible
* Describe improvement opportunities when you conclude

### Other Notes ###

* Animations and interaction effects are up to you
* Commit to this repo as you go - please make a branch for your work where we will evaluate the final thing.
* We will be asking about your approach and how long it took in the end 

### Assets ###

Here’s some assets to get you going:

* Figma https://www.figma.com/file/doT5l1vwoFHXJZjv7bkHtZ/Challenge-Progress-Stepper
* Saira Condensed font https://fonts.google.com/specimen/Saira+Condensed
* Asap font https://fonts.google.com/specimen/Asap





